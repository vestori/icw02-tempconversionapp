//
//  ViewController.m
//  TeamProject1 - Temp Converter
//
//  Created by Orlando Gotera on 9/27/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *sgmSelector;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderValue;
@property (weak, nonatomic) IBOutlet UISlider *sldTemp;
@property (weak, nonatomic) IBOutlet UILabel *lblConversionResult;
@property (weak, nonatomic) IBOutlet UILabel *lblMin;
@property (weak, nonatomic) IBOutlet UILabel *lblMax;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sldTemp.value = ((self.sldTemp.minimumValue + self.sldTemp.maximumValue) / 2 );
    self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Fahrenheit", self.sldTemp.value];
    self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Celsius", ((self.sldTemp.value - 32) / 1.8)];
    self.lblMin.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.minimumValue];
    self.lblMax.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.maximumValue];

}

//Method to update the lables upon change on segment selection
-(void)updateLabels: (int) selection {
    if (selection == 0) {
        self.sldTemp.minimumValue = 32;
        self.sldTemp.maximumValue = 212;
        self.sldTemp.value = ((self.sldTemp.minimumValue + self.sldTemp.maximumValue) / 2 );
        self.lblMin.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.minimumValue];
        self.lblMax.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.maximumValue];
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Fahrenheit", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Celsius", ((self.sldTemp.value - 32) / 1.8)];
    } else if (selection == 1) {
        self.sldTemp.minimumValue = 0;
        self.sldTemp.maximumValue = 100;
        self.sldTemp.value = ((self.sldTemp.minimumValue + self.sldTemp.maximumValue) / 2 );
        self.lblMin.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.minimumValue];
        self.lblMax.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.maximumValue];
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Celsius", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Fahrenheit", ((self.sldTemp.value * 1.8) + 32)];
    } else if (selection == 2) {
        self.sldTemp.minimumValue = 32;
        self.sldTemp.maximumValue = 212;
        self.sldTemp.value = ((self.sldTemp.minimumValue + self.sldTemp.maximumValue) / 2 );
        self.lblMin.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.minimumValue];
        self.lblMax.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.maximumValue];
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Fahrenheit", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Kelvin", ((self.sldTemp.value + 459.67) * .55555)];
    } else if (selection == 3) {
        self.sldTemp.minimumValue = 273.15;
        self.sldTemp.maximumValue = 373.15;
        self.sldTemp.value = ((self.sldTemp.minimumValue + self.sldTemp.maximumValue) / 2 );
        self.lblMin.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.minimumValue];
        self.lblMax.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.maximumValue];
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Kelvin", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Fahrenheit", (1.8 * (self.sldTemp.value - 273) + 32)];
    } else if (selection == 4) {
        self.sldTemp.minimumValue = 273.15;
        self.sldTemp.maximumValue = 373.15;
        self.sldTemp.value = ((self.sldTemp.minimumValue + self.sldTemp.maximumValue) / 2 );
        self.lblMin.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.minimumValue];
        self.lblMax.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.maximumValue];
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Kelvin", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Celsius", (self.sldTemp.value - 273.15)];
    } else if (selection == 5) {
        self.sldTemp.minimumValue = 0;
        self.sldTemp.maximumValue = 100;
        self.sldTemp.value = ((self.sldTemp.minimumValue + self.sldTemp.maximumValue) / 2 );
        self.lblMin.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.minimumValue];
        self.lblMax.text = [NSString stringWithFormat:@"%d", (int)self.sldTemp.maximumValue];
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Celsius", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Kelvin", (self.sldTemp.value + 273.15)];
    }
}

//Method that calculates the temperature and updates labels based on input from slider.
-(void)calculateTemp: (int) selection {
    if(selection == 0) {
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Fahrenheit", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Celsius", ((self.sldTemp.value - 32) / 1.8)];
    } else if (selection == 1) {
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Celsius", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Fahrenheit", ((self.sldTemp.value * 1.8) + 32)];
    } else if (selection == 2) {
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Fahrenheit", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Kelvin", (self.sldTemp.value + 459.67) * .55555];
    } else if (selection == 3) {
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Kelvin", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Fahrenheit", (1.8 * (self.sldTemp.value - 273) + 32)];
    } else if (selection == 4) {
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Kelvin", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Celsius", (self.sldTemp.value - 273.15)];
    } else if (selection == 5) {
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Celsius", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Kelvin", (self.sldTemp.value + 273.15)];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sldSelectTemp:(id)sender {
    if (self.sgmSelector.selectedSegmentIndex == 0) {
        [self calculateTemp:0];
    } else if (self.sgmSelector.selectedSegmentIndex == 1) {
        [self calculateTemp:1];
    } else if (self.sgmSelector.selectedSegmentIndex == 2) {
        [self calculateTemp:2];
    } else if (self.sgmSelector.selectedSegmentIndex == 3) {
        [self calculateTemp:3];
    } else if (self.sgmSelector.selectedSegmentIndex == 4) {
        [self calculateTemp:4];
    } else if (self.sgmSelector.selectedSegmentIndex == 5) {
        [self calculateTemp:5];
    }
    
}
- (IBAction)sgmSegmentChange:(id)sender {
    if (self.sgmSelector.selectedSegmentIndex == 0) {
        [self updateLabels:0];
    } else if (self.sgmSelector.selectedSegmentIndex == 1){
        [self updateLabels:1];
    } else if (self.sgmSelector.selectedSegmentIndex == 2){
        [self updateLabels:2];
    } else if (self.sgmSelector.selectedSegmentIndex == 3) {
        [self updateLabels:3];
    } else if (self.sgmSelector.selectedSegmentIndex == 4) {
        [self updateLabels:4];
    } else if (self.sgmSelector.selectedSegmentIndex == 5) {
        [self updateLabels:5];
    }
}


@end
